FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code

ENV ESPOCRM_OLD_VERSION 5.8.3
ENV ESPOCRM_VERSION 5.8.4

# latest code
RUN cd /tmp && \
    wget https://www.espocrm.com/downloads/EspoCRM-${ESPOCRM_VERSION}.zip && \
    unzip EspoCRM-${ESPOCRM_VERSION}.zip -d /tmp && \
    mkdir /app/code/${ESPOCRM_VERSION} && \
    mv /tmp/EspoCRM-${ESPOCRM_VERSION}/* /app/code/${ESPOCRM_VERSION} && \
    rm -rf EspoCRM-${ESPOCRM_VERSION}.zip /tmp/EspoCRM-${ESPOCRM_VERSION} && \
    ln -sf /app/code/${ESPOCRM_VERSION} /app/code/current && \
    chown -R www-data:www-data /app/code/${ESPOCRM_VERSION}

WORKDIR /app/code/current

RUN rm -rf /app/code/current/data && ln -s /app/data/data /app/code/current/data && \
    rm -rf /app/code/current/custom && ln -s /app/data/custom /app/code/current/custom && \
    rm -rf /app/code/current/client/custom && ln -s /app/data/client/custom /app/code/current/client/custom && \
    mv /app/code/current/application/Espo/Modules /app/code/current/application/Espo/Modules.orig && ln -s /app/data/modules/application /app/code/current/application/Espo/Modules && \
    mv /app/code/current/client/modules /app/code/current/client/modules.orig && ln -s /app/data/modules/client /app/code/current/client/modules

# previous code
RUN cd /tmp && \
    wget https://www.espocrm.com/downloads/EspoCRM-${ESPOCRM_OLD_VERSION}.zip && \
    unzip EspoCRM-${ESPOCRM_OLD_VERSION}.zip -d /tmp && \
    mkdir /app/code/${ESPOCRM_OLD_VERSION} && \
    mv /tmp/EspoCRM-${ESPOCRM_OLD_VERSION}/* /app/code/${ESPOCRM_OLD_VERSION} && \
    rm -rf EspoCRM-${ESPOCRM_OLD_VERSION}.zip /tmp/EspoCRM-${ESPOCRM_OLD_VERSION} && \
    ln -sf /app/code/${ESPOCRM_OLD_VERSION} /app/code/old && \
    chown -R www-data:www-data /app/code/${ESPOCRM_OLD_VERSION}

RUN rm -rf /app/code/old/data && ln -s /app/data/data /app/code/old/data && \
    rm -rf /app/code/old/custom && ln -s /app/data/custom /app/code/old/custom && \
    rm -rf /app/code/old/client/custom && ln -s /app/data/client/custom /app/code/old/client/custom

# Upgrade package
RUN wget https://www.espocrm.com/downloads/upgrades/EspoCRM-upgrade-${ESPOCRM_OLD_VERSION}-to-${ESPOCRM_VERSION}.zip -O /app/code/upgrade.zip

# The VOIP integration packages installs some files in the root (https://forum.cloudron.io/topic/1874/espocrm-extension-installation-failure)
RUN ln -s /app/data/voip-integration/starface.php /app/code/current/starface.php && \
    ln -s /app/data/voip-integration/voip-service.php /app/code/current/voip-service.php && \
    ln -s /app/data/voip-integration/voip.php /app/code/current/voip.php && \
    chown www-data:www-data --no-dereference /app/code/current/starface.php /app/code/current/voip-service.php /app/code/current/voip.php

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/espocrm.conf /etc/apache2/sites-enabled/espocrm.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
# https://www.espocrm.com/blog/server-configuration-for-espocrm/
RUN a2enmod rewrite
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP max_execution_time 180 && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_input_time 180 && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 50M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 50M && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/espocrm/sessions

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
