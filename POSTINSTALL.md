This app integrates with Cloudron user management.

A default admin user has been setup with the following credentials:

```
username: admin
password: changeme
```

**Note:** Please change the password immediately after installation.

