This app packages EspoCRM version <upstream>5.8.4</upstream>.

EspoCRM is an Open Source CRM (Customer Relationship Management) software that
allows you to see, enter and evaluate all your company relationships regardless
of the type. People, companies or opportunities - all in an easy and intuitive
interface.

## Features

* Web based CRM. One version for all your devices.
* Free of charge and open source CRM distributed under GPLv3.
* Tested with most hosting providers.
* New features every 2 months.
* Responsive Design and latest Web Technologies.
* Lightning fast.
* No user limitation.
* Cloud and On-Premise version available.
* Easy to customize.
* Open architecture for third party integration via API.

