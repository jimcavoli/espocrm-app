#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    util = require('util'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    var email, token;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
        });
    }

    function login(username, password, done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.sleep(10000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.id('field-userName')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.id('field-userName')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.id('field-password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.id('btn-login')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//span[text()="Opportunities"]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function adminLogin(done) {
        login('admin', 'changeme', done);
    }

    var contactId;
    function createContact(done) {
        browser.get('https://' + app.fqdn + '/#Account/create').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//input[@data-name="name"]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@data-name="name"]')).sendKeys('cloudroncontact');
        }).then(function () {
            return browser.findElement(by.xpath('//button[text()="Save"]')).click();
        }).then(function () {
            return browser.wait(function () {
                return browser.getCurrentUrl().then(function (url) {
                    contactId = url.slice(url.lastIndexOf('/')+1);
                    return url.startsWith('https://' + app.fqdn + '/#Account/view/');
                });
            }, TIMEOUT);
        }).then(function () {
            console.log(`Contact ID is ${contactId}`);
            done();
        });
    }

    function contactExists(done) {
        browser.get(`https://${app.fqdn}/#Account/view/${contactId}`).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//div[text()="cloudroncontact"]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function logout(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[@id="nav-menu-dropdown"]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//a[@id="nav-menu-dropdown"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[text()="Log Out"]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//a[text()="Log Out"]')).click();
        }).then(function () {
            return browser.sleep(4000);
        }).then(function () {
            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function (done) {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        setTimeout(done, 5000); // wait for sometime for the setup to finish
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    // this allows a normal user to access all views. otherwise, we have to create roles etc
    it('can disable acl', function () {
        execSync(`cloudron exec --app ${app.fqdn} -- sed -e 's/.*aclStrictMode.*/    "aclStrictMode" => false,/g' -i /app/data/data/config.php`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can create contact', createContact);
    it('can check contact', contactExists);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        setTimeout(done, 5000);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('can backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function (done) {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        setTimeout(done, 5000);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('move to different location', function () {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app for update', function (done) {
        execSync('cloudron install --appstore-id com.espocrm.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
        setTimeout(done, 5000); // wait for sometime for the setup to finish
    });
    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can disable acl', function () {
        execSync(`cloudron exec --app ${app.fqdn} -- sed -e 's/.*aclStrictMode.*/    "aclStrictMode" => false,/g' -i /app/data/data/config.php`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can restart app for setting to take effect', function (done) {
        execSync('cloudron restart --app ' + app.id);
        setTimeout(done, 5000);
    });
    it('can login', login.bind(null, username, password));
    it('can create contact', createContact);
    it('can logout', logout);

    it('can update', function (done) {
        execSync('cloudron update --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        setTimeout(done, 10000);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can check contact', contactExists);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});

